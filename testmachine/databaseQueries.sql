CREATE table MACHINE(
	id IDENTITY,
	machineName varchar(20) UNIQUE,
	machineIP varchar(15) UNIQUE,
	OS varchar(10),
	status varchar(20),
	build varchar(20),
	
	CONSTRAINT pk_machine_id PRIMARY KEY (id)
);


insert into MACHINE (machineName, machineIP, OS, status, build) values ('Sonu', '10.177.99.201', 'Linux', 'Available', 'm1.2345e');
insert into MACHINE (machineName, machineIP, OS, status, build) values ('Kumar', '10.177.99.202', 'Windows', 'Booked', 'm1.2345e'); 
insert into MACHINE (machineName, machineIP, OS, status, build) values ('Sah', '10.177.99.203', 'Linux', 'Do Not Disturb', 'm1.2345e');
insert into MACHINE (machineName, machineIP, OS, status, build) values ('S K Sah', '10.177.99.204', 'Windows', 'LFC Required', 'm1.2345e');
insert into MACHINE (machineName, machineIP, OS, status, build) values ('Test1', '10.177.99.205', 'Linux', 'Available', 'm1.2345e');
insert into MACHINE (machineName, machineIP, OS, status, build) values ('Test2', '10.177.99.206', 'Windows', 'Booked', 'm345e'); 
insert into MACHINE (machineName, machineIP, OS, status, build) values ('Test3', '10.177.99.207', 'Linux', 'Do Not Disturb', 'm5e');
insert into MACHINE (machineName, machineIP, OS, status, build) values ('Test4', '10.177.99.208', 'Windows', 'LFC Required', 'me');
insert into MACHINE (machineName, machineIP, OS, status, build) values ('Test11', '10.177.99.211', 'Linux', 'Available', 'm1.2345e');
insert into MACHINE (machineName, machineIP, OS, status, build) values ('Test22', '10.177.99.212', 'Windows', 'Booked', 'm345e'); 
insert into MACHINE (machineName, machineIP, OS, status, build) values ('Test33', '10.177.99.213', 'Linux', 'Do Not Disturb', 'm5e');
insert into MACHINE (machineName, machineIP, OS, status, build) values ('Test44', '10.177.99.214', 'Windows', 'LFC Required', 'me');
