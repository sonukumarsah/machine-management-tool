package com.sonu.testmachine.test;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.sonu.testmachine.dao.MachineDAO;
import com.sonu.testmachine.dao.UserDAO;
import com.sonu.testmachine.dto.Machine;
import com.sonu.testmachine.dto.User;

public class UserTest {
	
private static AnnotationConfigApplicationContext context;
	
	private static UserDAO userDAO;
	private static MachineDAO machineDAO;
	
	private User user;
	
	private Machine machine;
	
	
	@BeforeClass
	public static void init() {
		context=new AnnotationConfigApplicationContext();
		context.scan("com.sonu.testmachine");
		context.refresh();
		userDAO=(UserDAO) context.getBean("userDAO");
		machineDAO=(MachineDAO) context.getBean("machineDAO");
	}
	
	/*@Test
	public void testAddUser() {
		user = new User() ;
		user.setName("Sonu Kumar Sah");
		user.setContactNumber("7019437943");
		user.setPassword("123456");
		user.setRole("Admin");
		user.setEmail("sonukumarsah@gmail.com");
		
		// add the user
		assertEquals("Successfully add the user inside the table!", true, userDAO.add(user));
		
		user = new User() ;
		user.setName("User1");
		user.setContactNumber("9876543210");
		user.setPassword("123456");
		user.setRole("User");
		user.setEmail("user@gmail.com");
		user.setMachine(machine);
		
		// add the user
		assertEquals("Successfully add the user inside the table!", true, userDAO.add(user));
	}*/
	/*@Test
	public void testGetUser() {
		user=userDAO.get(2);
		assertEquals("Successfully fetch a data from the Table!","user@gmail.com",user.getEmail());
	}*/
	/*@Test
	public void testUpdateUser() {
		user=userDAO.get(2);
		user.setEmail("user1@gmail.com");
		assertEquals("Successfully Update a data from the Table!",true,userDAO.update(user));
	}*/
	/*@Test
	public void testDeleteUser() {
		user=userDAO.get(4);
		assertEquals("Successfully delete a data from the Table!",true,userDAO.delete(user));
	}*/
	/*@Test
	public void testSearchByEmail() {
		user=userDAO.getByEmail("user1@gmail.com");
		user.setEmail("hr@gmail.com");
		assertEquals("Successfully Update a data from the Table!",true,userDAO.update(user));
	}*/
	/*@Test
	public void testSearchByContact() {
		user=userDAO.getByContactNumber("9876543210");
		user.setContactNumber("9019399555");
		assertEquals("Successfully Update a data from the Table!",true,userDAO.update(user));
	}*/
	
	@Test
	public void testCRUDUser(){
		user = new User() ;
		user.setName("Sonu Kumar Sah");
		user.setContactNumber("7019437943");
		user.setPassword("123456");
		user.setRole("Admin");
		user.setEmail("sonukumarsah@gmail.com");
		user.setMachine(machine);
		
		// add the user
		assertEquals("Successfully add the user inside the table!", true, userDAO.add(user));
		
		user = new User() ;
		user.setName("User1");
		user.setContactNumber("9876543210");
		user.setPassword("123456");
		user.setRole("User");
		user.setEmail("user@gmail.com");
		user.setMachine(machine);
		
		// add the user
		assertEquals("Successfully add the user inside the table!", true, userDAO.add(user));
		
		user = new User() ;
		user.setName("Sah");
		user.setContactNumber("9019399000");
		user.setPassword("123456");
		user.setRole("Admin");
		user.setEmail("sah@gmail.com");
		user.setMachine(machine);
		
		// add the user
		assertEquals("Successfully add the user inside the table!", true, userDAO.add(user));
		
		
		user=userDAO.get(2);
		assertEquals("Successfully fetch a data from the Table!","user@gmail.com",user.getEmail());
		
		user=userDAO.get(2);
		user.setEmail("user1@gmail.com");
		assertEquals("Successfully Update a data from the Table!",true,userDAO.update(user));
		
		user=userDAO.get(1);
		assertEquals("Successfully delete a data from the Table!",true,userDAO.delete(user));
		
		user=userDAO.getByEmail("user1@gmail.com");
		user.setEmail("hr@gmail.com");
		assertEquals("Successfully Update a data from the Table!",true,userDAO.update(user));
		
		user=userDAO.getByContactNumber("9876543210");
		user.setContactNumber("9019399555");
		assertEquals("Successfully Update a data from the Table!",true,userDAO.update(user));
		
		assertEquals("Successfully fetch all data from the Table!",1,userDAO.list().size());
		
		user = userDAO.get(2);
		machine = machineDAO.get(2);
		machine.setUser(user);
		machineDAO.update(machine);
		user.setMachine(machine);
		userDAO.update(user);
		
		assertEquals("Successfully fetch all data from the Table!",true,userDAO.listAllMachineByUser(user)!=null);
		
		
	}
	/*@Test
	public void testListUser(){
		assertEquals("Successfully fetch all data from the Table!",1,userDAO.list().size());
		
	}*/
	/*@Test
	public void testListAllMachineByUser(){
		
		user = userDAO.get(2);
		machine = machineDAO.get(2);
		machine.setUser(user);
		machineDAO.update(machine);
		user.setMachine(machine);
		userDAO.update(user);
		
		assertEquals("Successfully fetch all data from the Table!",true,userDAO.listAllMachineByUser(user)!=null);
		
		
	}*/

}
