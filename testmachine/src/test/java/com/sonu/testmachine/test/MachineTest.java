package com.sonu.testmachine.test;

import static org.junit.Assert.assertEquals;

import javax.persistence.EntityManager;

import org.hibernate.query.Query;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.sonu.testmachine.dao.MachineDAO;
import com.sonu.testmachine.dto.Machine;
import com.sonu.testmachine.dto.User;

public class MachineTest {
	
	private static AnnotationConfigApplicationContext context;
	
	private static MachineDAO machineDAO;
	
	private Machine machine;
	
	private User user;
	
	@BeforeClass
	public static void init(){
		context=new AnnotationConfigApplicationContext();
		context.scan("com.sonu.testmachine");
		context.refresh();
		machineDAO=(MachineDAO) context.getBean("machineDAO");
	}
	
	/*@Test
	public void testAddMachine(){
		machine=new Machine();
		machine.setMachineName("Sonu"); 
		machine.setMachineIP("10.177.208.210");
		machine.setOS("Linux");
		machine.setStatus("Available");
		machine.setBuild("M2");
		machine.setUser(user);
		assertEquals("Successfully added a data inside the Table!",true,machineDAO.add(machine) );
		machine=new Machine();
		machine.setMachineName("Kumar"); 
		machine.setMachineIP("10.177.208.211");
		machine.setOS("Windows");
		machine.setStatus("Booked");
		machine.setBuild("M4");
		machine.setUser(user);
		
		assertEquals("Successfully added a data inside the Table!",true,machineDAO.add(machine) );
	}*/
	
	/*@Test
	public void testGetMachine(){
		machine=machineDAO.get(2);
		
		assertEquals("Successfully fetch a data from the Table!","Kumar",machine.getMachineName() );
	}*/
	/*@Test
	public void testUpdateMachine(){
		machine=machineDAO.get(2);
		machine.setMachineName("Sah");
		
		assertEquals("Successfully updated a data in the Table!",true,machineDAO.update(machine) );
	}*/
	/*@Test
	public void testDeleteMachine(){
		machine=machineDAO.get(2);
		assertEquals("Successfully deleted a data from the Table!",true,machineDAO.delete(machine) );
	}*/
	
/*	@Test
	public void testListMachine(){
		assertEquals("Successfully fetch all data from the Table!",true,machineDAO.list()!=null );
	}*/
	
	@Test
	public void testCRUDMachine(){
		//Adding a data
		machine=new Machine();
		machine.setMachineName("Kumar"); 
		machine.setMachineIP("10.177.208.209");
		machine.setOS("Linux");
		machine.setStatus("Booked");
		machine.setBuild("M2");
		
		assertEquals("Successfully added a data inside the Table!",true,machineDAO.add(machine) );
		
		machine=new Machine();
		machine.setMachineName("Sah"); 
		machine.setMachineIP("10.177.208.211");
		machine.setOS("Windows");
		machine.setStatus("LFC Required");
		machine.setBuild("M3");
		
		assertEquals("Successfully added a data inside the Table!",true,machineDAO.add(machine) );
		
		// fetch a data
		machine=machineDAO.get(1);
		assertEquals("Successfully fetch a data from the Table!","Kumar",machine.getMachineName() );
		// update a data
		machine=machineDAO.get(2);
		machine.setMachineName("Sonu kumar Sah");
		
		assertEquals("Successfully updated a data in the Table!",true,machineDAO.update(machine) );
		
		// delete a data
		machine=machineDAO.get(1);
		assertEquals("Successfully deleted a data from the Table!",true,machineDAO.delete(machine) );
		
		// fetch all list
		
		assertEquals("Successfully fetch all data from the Table!",true,machineDAO.list()!=null );
	}

}
