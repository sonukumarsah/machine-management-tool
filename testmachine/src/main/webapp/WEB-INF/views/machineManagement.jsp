<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<div class="container-fluid">
	<div class="card">
				<h3 class="card-header text-center font-weight-bold text-uppercase py-4">Machine Management</h3>
	</div>
	<div style = "text-align:right">
	     <b><a class="navbar-brand text-right" href="${contextRoot}/newMachine">Add New Machine</a></b>
	</div>
	<div class="card mb-4">
	<div class="table-responsive">
	<sf:form method="post" modelAttribute="upadateMachine" action="${pageContext.request.contextPath}/${id}/save">
		<table class="table table-bordered" id="dataTable" width="100%"
			cellspacing="0">
			<thead>
				<tr>
					<th>Machine Name</th>
					<th>IP</th>
					<th>OS</th>
					<th>Status</th>
					<th>Build Information</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
					<c:if test="${status =='Available'}">
						<tr>
							<td><input type="text" name="machineName" value="${machineName}" required/></td>
							<td><input type="text" name="machineIP" value="${machineIP}" required/></td>
							<td><input type="text" name="OS" value="${OS}" required/></td>
							<td>
							<select name="status">
 								<option value="${status}">${status}</option>
 								<option value="Booked">Booked</option>
    					    	<option value="Do Not Disturb">Do Not Disturb</option>
    					    	<option value="LFC Required">LFC Required</option>
    					    	</select>
							</td>
							<td><input type="text" name="build" value="${build}" required/></td>
							<td><input type="submit" value="Save"/></td>
							<td><a href="${contextRoot}/show/${id}/delete">Remove</a></td>
						</tr>
					</c:if>
					<c:if test="${status =='Booked'}">
						<tr>
							<td><input type="text" name="machineName" value="${machineName}" required/></td>
							<td><input type="text" name="machineIP" value="${machineIP}" required/></td>
							<td><input type="text" name="OS" value="${OS}" required/></td>
							<td>
							<select name="status">
 								<option value="${status}">${status}</option>
 								<option value="Available">Available</option>
    					    	<option value="Do Not Disturb">Do Not Disturb</option>
    					    	<option value="LFC Required">LFC Required</option>
    					    	</select>
							</td>
							<td><input type="text" name="build" value="${build}" required/></td>
							<td><input type="submit" value="Save"/></td>
							<td><a href="${contextRoot}/show/${id}/delete">Remove</a></td>
						</tr>
					</c:if>
					<c:if test="${status =='LFC Required'}">
						<tr>
							<td><input type="text" name="machineName" value="${machineName}" required/></td>
							<td><input type="text" name="machineIP" value="${machineIP}" required/></td>
							<td><input type="text" name="OS" value="${OS}" required/></td>
							<td>
							<select name="status">
 								<option value="${status}">${status}</option>
 								<option value="Available">Available</option>
    					    	<option value="Booked">Booked</option>
    					    	<option value="Do Not Disturb">Do Not Disturb</option>
    					    	</select>
							</td>
							<td><input type="text" name="build" value="${build}" required/></td>
							<td><input type="submit" value="Save"/></td>
							<td><a href="${contextRoot}/show/${id}/delete">Remove</a></td>
						</tr>
					</c:if>
					<c:if test="${status =='Do Not Disturb'}">
						<tr>
							<td><input type="text" name="machineName" value="${machineName}" required/></td>
							<td><input type="text" name="machineIP" value="${machineIP}" required/></td>
							<td><input type="text" name="OS" value="${OS}" required/></td>
							<td>
							<select name="status">
 								<option value="${status}">${status}</option>
 								<option value="Available">Available</option>
    					    	<option value="Booked">Booked</option>
    					    	<option value="LFC Required">LFC Required</option>
    					    	</select>
							</td>
							<td><input type="text" name="build" value="${build}" required/></td>
							<td><input type="submit" value="Save"/></td>
							<td><a href="${contextRoot}/show/${id}/delete">Remove</a></td>
						</tr>
					</c:if>
					</tbody>
					</table>
					</sf:form>
	</div>
</div>