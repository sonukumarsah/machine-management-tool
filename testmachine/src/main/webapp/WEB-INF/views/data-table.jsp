	<div style = "text-align:center">
	     <b><h3 class="navbar-brand">Machine Data Table</h3></b>
	     <div style = "text-align:right; float:right">
	     <b><a class="navbar-brand" href="${contextRoot}/newMachine">Add New Machine</a></b>
	</div>
	</div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Machine Name</th>
                                                <th>IP</th>
                                                <th>OS</th>
                                                <th>Status</th>
                                                <th>Build Information</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <c:forEach items="${machineList}" var="lists">
                                            <tr>
                                                <td>${lists.machineName}</td>
                                                <td>${lists.machineIP}</td>
                                                <td>${lists.OS}</td>
                                                <td>${lists.status}</td>
                                                <td>${lists.build}</td>
                                                <td><a href="${contextRoot}/show/${lists.id}">Edit</a></td>
                                            </tr>
                                            </c:forEach>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>