<%@page import="com.fasterxml.jackson.annotation.JsonInclude.Include"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="card-body">
<div class="card">
				<h3 class="card-header text-center font-weight-bold text-uppercase py-4">Machine Data Table</h3>
				<div style = "text-align:right; float:right">
	     <b><a class="navbar-brand" href="${contextRoot}/newMachine">Add New Machine</a></b>
	</div>
	</div>
	<div class="table-responsive">
		<table class="table table-bordered" id="dataTable" width="100%"
			cellspacing="0">
			<thead>
				<tr>
					<th>Machine Name</th>
					<th>IP</th>
					<th>OS</th>
					<th>Status</th>
					<th>Build Information</th>
					<th>Activity</th>
				</tr>
			</thead>
			<tbody>
			<c:if test="${userClickAvailableMachine ==true }">
				<c:forEach items="${machineList}" var="lists">
					<c:if test="${lists.status =='Available'}">
						<tr>
							<td>${lists.machineName}</td>
							<td>${lists.machineIP}</td>
							<td>${lists.OS}</td>
							<td>${lists.status}</td>
							<td>${lists.build}</td>
							<td><a href="${contextRoot}/show/${lists.id}">Edit</a></td>
                                            </tr>
					</c:if>
				</c:forEach>
				</c:if>
				<c:if test="${userClickBookedMachine ==true }">
				<c:forEach items="${machineList}" var="lists">
					<c:if test="${lists.status =='Booked'}">
						<tr>
							<td>${lists.machineName}</td>
							<td>${lists.machineIP}</td>
							<td>${lists.OS}</td>
							<td>${lists.status}</td>
							<td>${lists.build}</td>
							<td><a href="${contextRoot}/show/${lists.id}">Edit</a></td>
                                            </tr>
					</c:if>
				</c:forEach>
				</c:if>
				<c:if test="${userClickDNDMachine ==true }">
				<c:forEach items="${machineList}" var="lists">
					<c:if test="${lists.status =='Do Not Disturb'}">
						<tr>
							<td>${lists.machineName}</td>
							<td>${lists.machineIP}</td>
							<td>${lists.OS}</td>
							<td>${lists.status}</td>
							<td>${lists.build}</td>
							<td><a href="${contextRoot}/show/${lists.id}">Edit</a></td>
                                            </tr>
					</c:if>
				</c:forEach>
				</c:if>
				<c:if test="${userClickLFCMachine ==true }">
				<c:forEach items="${machineList}" var="lists">
					<c:if test="${lists.status =='LFC Required'}">
						<tr>
							<td>${lists.machineName}</td>
							<td>${lists.machineIP}</td>
							<td>${lists.OS}</td>
							<td>${lists.status}</td>
							<td>${lists.build}</td>
							<td><a href="${contextRoot}/show/${lists.id}">Edit</a></td>
                                            </tr>
					</c:if>
				</c:forEach>
				</c:if>
			</tbody>
		</table>
	</div>
</div>