<div class="container-fluid">
	<div class="row">
		<div class="col-xl-3 col-md-6">
			<div class="card bg-danger text-white mb-4">
				<div class="card-body">Do Not Disturb</div>
				<div
					class="card-footer d-flex align-items-center justify-content-between">
					<a class="small text-white stretched-link" href="${contextRoot}/show/DNDMachine">View
						Details</a>
					<div class="small text-white">
						<i class="fas fa-angle-right"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="card mb-4">
		<%@include file="machineData.jsp"%>
	</div>
</div>