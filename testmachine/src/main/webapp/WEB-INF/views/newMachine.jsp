<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<div class="container-fluid">
	<div class="card">
				<h3 class="card-header text-center font-weight-bold text-uppercase py-4">Add New Machine</h3>
	</div>
	<div class="card mb-4">
	<div class="table-responsive">
	<sf:form method="post" modelAttribute="newMachine" action="${pageContext.request.contextPath}/newMachine">
		<table class="table table-bordered" id="dataTable" width="100%"
			cellspacing="0">
			<thead>
				<tr>
					<th>Machine Name</th>
					<th>IP</th>
					<th>OS</th>
					<th>Status</th>
					<th>Build Information</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
						<tr>
							<td><input type="text" name="machineName" required/></td>
							<td><input type="text" name="machineIP" required/></td>
							<td><input type="text" name="OS" required/></td>
							<td>
							<select name="status">
 								<option value="Available">Available</option>
 								<option value="Booked">Booked</option>
    					    	<option value="Do Not Disturb">Do Not Disturb</option>
    					    	<option value="LFC Required">LFC Required</option>
    					    	</select>
							</td>
							<td><input type="text" name="build" required/></td>
							<td><input type="submit" value="Save"/></td>
						</tr>
					</tbody>
					</table>
					</sf:form>
	</div>
</div>