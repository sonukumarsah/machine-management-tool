<%@page import="com.fasterxml.jackson.annotation.JsonInclude.Include"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:url var="css" value="/css"/>
<spring:url var="js" value="/js"/>
<spring:url var="images" value="/images"/>

<c:set var="contextRoot" value ="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Test Machine Management Tool</title>
 <script>
	window.contextRoot="${contextRoot}"
 </script>
        <link href="${css}/styles.css" rel="stylesheet" />
        <link href="${css}/dataTables.bootstrap4.min.css" rel="stylesheet"/>
    </head>
    <body class="sb-nav-fixed">
            <!-- Navbar-->
            <%@include file="./shared/navbar.jsp" %>
        <!-- Layout Side Nav -->
        <%@include file="./shared/layout-side-nav.jsp" %>
            <div id="layoutSidenav_content">
                <main>
                    <!-- Page Content -->
                    <!-- Loading the home content -->
                    <c:if test="${userClickHome ==true }">
                    <script>
        				window.machineID='';
     				</script>
                    <%@include file="home.jsp" %>
                    </c:if>
                    
                    <!-- Load only when user clicks settings -->
                    <c:if test="${userClickSettings ==true }">
                    <%@include file="settings.jsp" %>
                    </c:if>
                    
                    <!-- Load only when user clicks Activity Log -->
                    <c:if test="${userClickActivity ==true }">
                    <%@include file="activity.jsp" %>
                    </c:if>
                    
                    <!-- Load only when user clicks Logout -->
                    <c:if test="${userClickLogout ==true }">
                    <%@include file="./shared/login.jsp" %>
                    </c:if>
                    <c:if test="${userClickAvailableMachine ==true }">
                    <script>
        				window.machineID='${machine.id}';
     				</script>
                    <%@include file="availableMachine.jsp" %>
                    </c:if>
                    <c:if test="${userClickBookedMachine ==true }">
                    <script>
        				window.machineID='${machine.id}';
     				</script>
                    <%@include file="bookedMachine.jsp" %>
                    </c:if>
                    <c:if test="${userClickDNDMachine ==true }">
                    <script>
        				window.machineID='${machine.id}';
     				</script>
                    <%@include file="DNDMachine.jsp" %>
                    </c:if>
                    <c:if test="${userClickLFCMachine ==true }">
                    <script>
        				window.machineID='${machine.id}';
     				</script>
                    <%@include file="LFCMachine.jsp" %>
                    </c:if>
                    <!-- Load only when user click on edit machine details -->
                    <c:if test="${userClickManageMachine ==true }">
                    <%@include file="machineManagement.jsp" %>
                    </c:if>
                    <!-- Load only when user click on edit machine details -->
                    <c:if test="${userClickUpadteMachine ==true }">
                    <%@include file="machineManagement.jsp" %>
                    </c:if>
                    <!-- Load only when user click on save machine details -->
                    <c:if test="${userClickSaveMachine ==true or userClickSaveNewMachine==true}">
                    <h2>Machine Details Updated Sussessfully</h2>
                    <div class="navbar-header">
	                <a class="navbar-brand" href="${contextRoot}/home">Home</a>
                    </c:if>
                    <!-- Load only when user click on edit machine details -->
                    <c:if test="${userClickDeleteMachine ==true }">
                    <h2>Machine Remove Sussessfully</h2>
                    <div class="navbar-header">
	                <a class="navbar-brand" href="${contextRoot}/home">Home</a>
	            </div>
                    </c:if>
                    <!-- Load only when user click on add new machine details -->
                    <c:if test="${userClickAddNewMachine ==true }">
                    <%@include file="newMachine.jsp" %>
                    </c:if>
                </main>
                <!-- Footer -->
                <%@include file="./shared/footer.jsp" %>
            </div>
        </div>
        <script src="${js}/jquery-3.4.1.min.js"></script>
        <script src="${js}/bootstrap.bundle.min.js"></script>
        <script src="${js}/scripts.js"></script>
        <script src="${js}/Chart.min.js"></script>
        <script src="${js}/chart-area-demo.js"></script>
        <script src="${js}/chart-bar-demo.js"></script>
        <script src="${js}/jquery.dataTables.min.js"></script>
        <script src="${js}/dataTables.bootstrap4.min.js"></script>
        <script src="${js}/datatables-demo.js"></script>
        <script src="${js}/all.min.js"></script>
    </body>
</html>