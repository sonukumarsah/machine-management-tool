<div class="container-fluid">
	<div class="row">
		<div class="col-xl-3 col-md-6">
			<div class="card bg-primary text-white mb-4">
				<div class="card-body">LFC Required</div>
				<div
					class="card-footer d-flex align-items-center justify-content-between">
					<a class="small text-white stretched-link" href="${contextRoot}/show/LFCMachine">View
						Details</a>
					<div class="small text-white">
						<i class="fas fa-angle-right"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="card mb-4">
		<%@include file="machineData.jsp"%>
	</div>
</div>