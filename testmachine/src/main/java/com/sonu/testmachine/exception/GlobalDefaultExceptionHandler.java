package com.sonu.testmachine.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class GlobalDefaultExceptionHandler {
	
	@ExceptionHandler(value = NoHandlerFoundException.class)
	public ModelAndView handlerNoHandlerFoundException(){
		ModelAndView mv=new ModelAndView("error");
		mv.addObject("errorTitle", "The page is not constructed");
		mv.addObject("errorDescription", "The page you are looking for is not available now!");
		mv.addObject("title", "404 Error Page");
		return mv;
	}
	@ExceptionHandler(value = MachineNotFoundException.class)
	public ModelAndView handlerMachineNotFoundException(){
		ModelAndView mv=new ModelAndView("error");
		mv.addObject("errorTitle", "This Machine is not constructed");
		mv.addObject("errorDescription", "Which Machine you are looking for that is not available right now!");
		mv.addObject("title", "Machine Unavailable");
		return mv;
	}
	@ExceptionHandler(value = Exception.class)
	public ModelAndView handlerException(Exception ex){
		ModelAndView mv=new ModelAndView("error");
		mv.addObject("errorTitle", "Contact your Administrator");
		mv.addObject("errorDescription", ex.toString());
		mv.addObject("title", "Error");
		return mv;
	}
	

}
