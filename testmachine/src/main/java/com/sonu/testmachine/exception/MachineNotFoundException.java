package com.sonu.testmachine.exception;

import java.io.Serializable;

public class MachineNotFoundException  extends Exception implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	public MachineNotFoundException() {
		// TODO Auto-generated constructor stub
		this("Machine is not Available");
	}
	public MachineNotFoundException(String message) {
		// TODO Auto-generated constructor stub
		this.message=System.currentTimeMillis() +": "+message;
	}
	public String getMessage() {
		return message;
	}
	

}
