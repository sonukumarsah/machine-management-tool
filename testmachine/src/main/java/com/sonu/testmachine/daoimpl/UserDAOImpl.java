package com.sonu.testmachine.daoimpl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sonu.testmachine.dao.UserDAO;
import com.sonu.testmachine.dto.Machine;
import com.sonu.testmachine.dto.User;

@Repository("userDAO")
@Transactional
public class UserDAOImpl implements UserDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public boolean add(User user) {
		try {			
			sessionFactory.getCurrentSession().persist(user);			
			return true;
		}
		catch(Exception ex) {
			return false;
		}
	}

	@Override
	public boolean update(User user) {
		try {			
			sessionFactory.getCurrentSession().update(user);			
			return true;
		}
		catch(Exception ex) {
			return false;
		}
	}

	@Override
	public boolean delete(User user) {
		try {			
			sessionFactory.getCurrentSession().delete(user);			
			return true;
		}
		catch(Exception ex) {
			return false;
		}
	}

	@Override
	public User get(int id) {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().get(User.class, Integer.valueOf(id));
	}

	@Override
	public User getByEmail(String email) {
		// TODO Auto-generated method stub
		String selectQuery = "FROM User WHERE email = :email";
		try {
		return sessionFactory
				.getCurrentSession()
					.createQuery(selectQuery,User.class)
						.setParameter("email",email)
							.getSingleResult();
		}
		catch(Exception ex) {
			return null;
		}
	}
	@Override
	public User getByContactNumber(String contact) {
		// TODO Auto-generated method stub
		String selectQuery = "FROM User WHERE contactNumber = :contact";
		try {
		return sessionFactory
				.getCurrentSession()
					.createQuery(selectQuery,User.class)
						.setParameter("contact",contact)
							.getSingleResult();
		}
		catch(Exception ex) {
			return null;
		}
	}

	@Override
	public List<User> list() {
			String selectAllUser="FROM User U WHERE U.role = :role";
			Query query=sessionFactory.getCurrentSession().createQuery(selectAllUser);
			query.setParameter("role", "Admin");
			return query.getResultList();
	}

	@Override
	public List<Machine> listAllMachineByUser(User user) {
		// TODO Auto-generated method stub
		String selectAllMachine="FROM Machine WHERE user = :user";
		
		try {
			return sessionFactory.getCurrentSession()
					.createQuery(selectAllMachine, Machine.class)
					.setParameter("user", user)
					.getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

}
