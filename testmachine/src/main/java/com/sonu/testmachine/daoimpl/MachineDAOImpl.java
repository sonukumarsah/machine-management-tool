package com.sonu.testmachine.daoimpl;

import java.util.List;

import org.hibernate.query.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sonu.testmachine.dao.MachineDAO;
import com.sonu.testmachine.dto.Machine;

@Repository("machineDAO")
@Transactional
public class MachineDAOImpl implements MachineDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	

	@Override
	public List<Machine> list() {
		// TODO Auto-generated method stub
		String selectAllMachine="FROM Machine";
		Query query=sessionFactory.getCurrentSession().createQuery(selectAllMachine);
		return query.getResultList();
	}

	@Override
	public Machine get(int id) {
		// enhanced for loop
		
		

		return sessionFactory.getCurrentSession().get(Machine.class, Integer.valueOf(id));
	}

	@Override
	@Transactional
	public boolean add(Machine machine) {
		// TODO Auto-generated method stub
		try {
			sessionFactory.getCurrentSession().persist(machine);
			
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean update(Machine machine) {
		try {
			sessionFactory.getCurrentSession().update(machine);
			
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	
	}

	@Override
	public boolean delete(Machine machine) {
		try {
			sessionFactory.getCurrentSession().delete(machine);
			
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}

}
