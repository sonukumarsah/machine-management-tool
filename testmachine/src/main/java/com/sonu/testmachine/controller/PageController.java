package com.sonu.testmachine.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.sonu.testmachine.dao.MachineDAO;
import com.sonu.testmachine.dto.Machine;

@RestController
public class PageController {
	
	private static final Logger logger=LoggerFactory.getLogger(PageController.class);
	
	@Autowired
	private MachineDAO machineDAO;
	
	@RequestMapping(value = {"/", "/home", "/index","/index.html"})
	public ModelAndView index(){
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("title", "Home");
		//Passing the list of machine
		logger.info("Inside the page controller index method -INFO");
		logger.debug("Inside the page controller index method -DEBUG");
		mv.addObject("machineList", machineDAO.list());
		mv.addObject("userClickHome", true);
		return mv;
	}
	@RequestMapping(value = {"/settings"})
	public ModelAndView settings(){
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("title", "Settings");
		mv.addObject("userClickSettings", true);
		return mv;
	}
	@RequestMapping(value = {"/activity"})
	public ModelAndView activity(){
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("title", "Activity Log");
		mv.addObject("userClickActivity", true);
		return mv;
	}
	@RequestMapping(value = {"/login","/show/login"})
	public ModelAndView logout(){
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("title", "Logout");
		mv.addObject("userClickLogout", true);
		return mv;
	}
	
	/*
	 * Method to load Machine list based on machine status
	 * */
	@RequestMapping(value = {"/show/availableMachine"})
	public ModelAndView availableMachine(){
		ModelAndView mv = new ModelAndView("page");
		//machineDAO to fetch a single entry based on status
		/*Machine machine=null;
		machine= machineDAO.get(status);
		mv.addObject("title", machine.getStatus());
		//Passing the list of machine
		mv.addObject("machineList", machineDAO.list());
		mv.addObject("machine", machine);*/
		mv.addObject("machineList", machineDAO.list());
		mv.addObject("userClickAvailableMachine", true);
		return mv;
	}
	@RequestMapping(value = {"/show/bookedMachine"})
	public ModelAndView bookedMachine(){
		ModelAndView mv = new ModelAndView("page");
		//machineDAO to fetch a single entry based on status
		/*Machine machine=null;
		machine= machineDAO.get(status);
		mv.addObject("title", machine.getStatus());
		//Passing the list of machine
		mv.addObject("machineList", machineDAO.list());
		mv.addObject("machine", machine);*/
		mv.addObject("machineList", machineDAO.list());
		mv.addObject("userClickBookedMachine", true);
		return mv;
	}
	@RequestMapping(value = {"/show/DNDMachine"})
	public ModelAndView DNDMachine(){
		ModelAndView mv = new ModelAndView("page");
		//machineDAO to fetch a single entry based on status
		/*Machine machine=null;
		machine= machineDAO.get(status);
		mv.addObject("title", machine.getStatus());
		//Passing the list of machine
		mv.addObject("machineList", machineDAO.list());
		mv.addObject("machine", machine);*/
		mv.addObject("machineList", machineDAO.list());
		mv.addObject("userClickDNDMachine", true);
		return mv;
	}
	@RequestMapping(value = {"/show/LFCMachine"})
	public ModelAndView LFCMachine(){
		ModelAndView mv = new ModelAndView("page");
		//machineDAO to fetch a single entry based on status
		/*Machine machine=null;
		machine= machineDAO.get(status);
		mv.addObject("title", machine.getStatus());
		//Passing the list of machine
		mv.addObject("machineList", machineDAO.list());
		mv.addObject("machine", machine);*/
		mv.addObject("machineList", machineDAO.list());
		mv.addObject("userClickLFCMachine", true);
		return mv;
	}
	@RequestMapping(value = "/show/{id}")
	public ModelAndView machineManagement(@PathVariable("id") int id){
		ModelAndView mv = new ModelAndView("page");
		Machine machine=null;
		machine= machineDAO.get(id);
		mv.addObject("machineName", machine.getMachineName());
		mv.addObject("machineIP", machine.getMachineIP());
		mv.addObject("OS", machine.getOS());
		mv.addObject("status", machine.getStatus());
		mv.addObject("build", machine.getBuild());
		mv.addObject("userClickManageMachine", true);
		return mv;
	}
	@RequestMapping(value = "/show/{id}/modify", method=RequestMethod.GET)
	public ModelAndView updateMachine(@PathVariable("id") int id){
		ModelAndView mv = new ModelAndView("page");
		Machine machine=null;
		machine= machineDAO.get(id);
		mv.addObject("ID", machine.getId());
		mv.addObject("machineName", machine.getMachineName());
		mv.addObject("machineIP", machine.getMachineIP());
		mv.addObject("OS", machine.getOS());
		mv.addObject("status", machine.getStatus());
		mv.addObject("build", machine.getBuild());
		mv.addObject("machine", machine);
		mv.addObject("userClickUpadteMachine", true);
		return mv;
	}
	@RequestMapping(value = "/{id}/save", method=RequestMethod.POST)
	public ModelAndView saveMachine(@ModelAttribute("upadateMachine") Machine newmachine){
		ModelAndView mv = new ModelAndView("page");
		machineDAO.update(newmachine);
		mv.addObject("userClickSaveMachine", true);
		return mv;
	}
	@RequestMapping(value = "/show/{id}/delete")
	public ModelAndView deleteMachine(@PathVariable("id") int id){
		ModelAndView mv = new ModelAndView("page");
		Machine machine=null;
		machine= machineDAO.get(id);
		machineDAO.delete(machine);
		mv.addObject("userClickDeleteMachine", true);
		return mv;
	}
	@RequestMapping(value = {"/newMachine"}, method=RequestMethod.GET)
	public ModelAndView addNewMachine(){
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("title", "New Machine");
		mv.addObject("userClickAddNewMachine", true);
		return mv;
	}
	@RequestMapping(value = {"/newMachine"}, method=RequestMethod.POST)
	public ModelAndView saveNewMachine(@ModelAttribute("newMachine") Machine newmachine){
		ModelAndView mv = new ModelAndView("page");
		machineDAO.add(newmachine);
		mv.addObject("userClickSaveNewMachine", true);
		return mv;
	}
	
	
}
