package com.sonu.testmachine.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sonu.testmachine.dao.MachineDAO;
import com.sonu.testmachine.dao.UserDAO;
import com.sonu.testmachine.dto.Machine;

@Controller
@RequestMapping("/json/data")
public class JsonDataController {
	
	@Autowired
	private MachineDAO machineDAO;
	
	@Autowired
	private UserDAO userDAO;
	
	@RequestMapping("/")
	@ResponseBody
	public List<Machine> getAllMachine(){
		return machineDAO.list();
	}
	

}
