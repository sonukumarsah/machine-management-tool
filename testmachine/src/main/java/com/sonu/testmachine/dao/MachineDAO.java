package com.sonu.testmachine.dao;

import java.util.List;

import com.sonu.testmachine.dto.Machine;

public interface MachineDAO {
	
	boolean add(Machine machine);
	boolean update(Machine machine);
	boolean delete(Machine machine);
	List<Machine> list();
	Machine get(int id);

}
