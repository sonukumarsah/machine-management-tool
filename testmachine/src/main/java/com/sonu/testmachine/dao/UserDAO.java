package com.sonu.testmachine.dao;

import java.util.List;

import com.sonu.testmachine.dto.Machine;
import com.sonu.testmachine.dto.User;

public interface UserDAO {
	boolean add(User user);
	boolean update(User user);
	boolean delete(User user);
	List<User> list();
	List<Machine> listAllMachineByUser(User user);
	User get(int id);
	User getByEmail(String email);
	User getByContactNumber(String contact);

}
